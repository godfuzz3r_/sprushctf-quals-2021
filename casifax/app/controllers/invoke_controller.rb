class Views
  def initialize(id)
    @id     = Integer(id)
    @article = Article.find(@id)
  end
  def increase
    @article.views += 1
    @article.save
    return '{"success": "true"}'
  end
  def getViews
    puts @article.views
    return @article.views
  end
end

class Likes
  def initialize(id)
    @id     = Integer(id)
    @article = Article.find(@id)
  end
  def setLike
    @article.likes += 1
    @article.save
    return '{"success": "true"}'
  end
  def setDislike
    @article.likes -= 1
    @article.save
    return '{"success": "true"}'
  end
  def getLikes
    puts @article.views
    return @article.views
  end
end

class InvokeController < ApplicationController
  def index
    className = params[:class].constantize.new(*params[:id])
    @out = className.method(params[:method]).call()
  end
end
