class ArticlesController < ApplicationController
  def index
    if params.has_key?(:search)
      @articles = Article.find_by_sql ["SELECT articles.* FROM articles WHERE articles.title like ? or articles.body like ?", params[:search], params[:search]]
    else
      @articles = Article.all
    end
  end

  def show
    @article = Article.find(params[:id])
  end
end
