# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

articles = Article.create([{id: 1,title: "Клятий рот цього казино",body:"Ужастное пригода вдалося бачити нашому журналісту цієї неділ в казино у Львові. У цьому казино колоди заряджають в кіосках!",views: 0,likes: 0,img: "/assets/ebanyy-rot-etogo-kazino.png"},{id: 2,title: "Secret casino",body:"Our reconnaissance squad managed to find a secret casino in Gelendzhik. The casino appears to be owned by local elites, but we have no idea who might own this.",views: 0,likes: 0,img: "/assets/secretcasino.png"},{id: 3,title: "Nobody came to the fan meeting...",body:"In this casino, decks are not charged at kiosks. However, this did not affect the fact that no one comes to this casino.",views: 0,likes: 0,img: "/assets/noonecame.png"},{id: 4,title: "Everybody wins in this game",body: "You can win millions in this game ... or you can lose. Anyway, bring us your money",views: 0,likes: 0,img: "/assets/wintotal.png"}])
