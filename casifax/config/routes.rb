Rails.application.routes.draw do
  root "articles#index"
  get "/articles", to: "articles#index"
  get "/articles/:id", to: "articles#show"

  get "/faq", to: "faq#index"
  get "/about", to: "about#index"
  get "/contacts", to: "contacts#index"

  get "/invokeMethod", to: "invoke#index"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
