from flask import Flask
from flask import render_template, abort, flash, request
from flask_bootstrap import Bootstrap

from flask_wtf import Form
from wtforms import StringField, SubmitField, BooleanField
from wtforms.validators import DataRequired

from flask_sqlalchemy import SQLAlchemy
from flask_login import current_user, login_required
from flask_security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, AnonymousUser

import os
import wtf_helpers
from datetime import datetime


app = Flask(__name__)
# in a real app, these should be configured through Flask-Appconfig
app.config['SECRET_KEY'] = "V3ry_str0ng_S3cret_Y0uC4nn0t4ccess_Th1s"
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///note.db'
app.config['SECURITY_REGISTERABLE'] = True
app.config['SECURITY_SEND_REGISTER_EMAIL'] = False
app.config['SECURITY_SEND_PASSWORD_CHANGE_EMAIL'] = False
app.config['SECURITY_SEND_PASSWORD_RESET_NOTICE_EMAIL'] = False
app.config['SECURITY_FLASH_MESSAGES'] = True
app.config['SECURITY_PASSWORD_SALT'] = "s4lt1svery1mp0rt4nt"

db = SQLAlchemy(app)

roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
                       db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))


class IdeaForm(Form):
    idea_name = StringField('Idea name', validators=[DataRequired()])
    submit_button = SubmitField('Add idea')


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    ideas = db.relationship('Idea', backref='user', lazy='dynamic')
    flask_app = app


class Idea(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    idea_name = db.Column(db.String(140))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __init__(self, idea, user):
        self.idea_name = idea
        self.user_id = user.id

    def __repr__(self):
        return '<Idea %r>' % self.id


user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)


db.create_all()

# check if admin exists and create it
adm = User.query.filter_by(email="admin@sprush.rocks").one_or_none()
if not adm:
    adm = User()
    adm.email = "admin@sprush.rocks"
    adm.password = "$2b$12$hP./wWeKGT6b/eR4HrBVOuAF6Ll39QLJC50Fy3z64YOui3rtBVvQW"
    adm.active = True
    db.session.add(adm)
    db.session.commit()

Bootstrap(app)
wtf_helpers.add_helpers(app)


@app.route("/")
def index():
    users = User.query
    return render_template("index.html", users=users)


def format_note(user_data, user):
    print(user.id)
    print(user.email)
    note = "{0.email} | {1} | " + user_data
    date = datetime.now().strftime("%H:%M:%S")
    return note.format(user, date)


@app.route("/ideas/<user_email>", methods=["GET", "POST"])
def ideas(user_email):
    user = User.query.filter_by(email=user_email).first_or_404()
    if user != current_user:
        return abort(403)

    if user == current_user:
        form = IdeaForm()
    else:
        form = None

    if form and form.validate_on_submit():
        idea = Idea(format_note(form.idea_name.data, user), user)
        form.idea_name.data = ""
        db.session.add(idea)
        db.session.commit()
        flash("Bet request was added successfully", "success")

    list_of_ideas = user.ideas
    return render_template("ideas.html", form=form, list_of_ideas=list_of_ideas, user_email=user_email)


@app.route("/admin")
def admin():
    if not current_user.is_authenticated:
        return abort(403)
    user = User.query.filter_by(id=current_user.id).one_or_none()
    if not user:
        return abort(403)

    if user.email != 'admin@sprush.rocks':
        return abort(403)

    return render_template("admin.html")

if __name__ == "__main__":
    app.run(debug=False, host='0.0.0.0')
