package com.collection.jackpot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class JackpotController {

	@RequestMapping(value = "/")
	public String index() {
		return "redirect:index.html";
	}

}
