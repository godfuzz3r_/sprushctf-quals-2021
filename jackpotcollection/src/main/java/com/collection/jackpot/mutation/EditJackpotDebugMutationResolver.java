package com.collection.jackpot.mutation;

import com.collection.jackpot.model.JackpotInput;
import com.collection.jackpot.model.Jackpot;
import com.collection.jackpot.service.JackpotService;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class EditJackpotDebugMutationResolver implements GraphQLMutationResolver {

    private final JackpotService jackpotService;

    public EditJackpotDebugMutationResolver(final JackpotService jackpotService) {
        this.jackpotService = jackpotService;
    }

    public Jackpot editJackpotDebug(final int id, final JackpotInput input) {
        return jackpotService.editJackpot(id, input);
    }
}
