package com.collection.jackpot.mutation;

import com.collection.jackpot.model.Jackpot;
import com.collection.jackpot.service.JackpotService;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class DeleteJackpotMutationResolver implements GraphQLMutationResolver {

    private final JackpotService jackpotService;

    public DeleteJackpotMutationResolver(final JackpotService jackpotService) {
        this.jackpotService = jackpotService;
    }

    public String deleteJackpot(final int id) {
        return jackpotService.deleteJackpot(id);
    }
}
