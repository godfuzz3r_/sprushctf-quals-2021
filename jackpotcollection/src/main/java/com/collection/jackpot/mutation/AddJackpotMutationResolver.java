package com.collection.jackpot.mutation;

import com.collection.jackpot.model.JackpotCollectionElem;
import com.collection.jackpot.service.JackpotService;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class AddJackpotMutationResolver implements GraphQLMutationResolver {

    private final JackpotService jackpotService;

    public AddJackpotMutationResolver(final JackpotService jackpotService) {
        this.jackpotService = jackpotService;
    }

    public JackpotCollectionElem addJackpot(final String input) {
        return jackpotService.addJackpot(input);
    }
}
