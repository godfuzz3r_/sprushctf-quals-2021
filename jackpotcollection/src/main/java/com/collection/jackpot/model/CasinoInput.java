package com.collection.jackpot.model;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CasinoInput {
    private String name;
    private String location;
    private int rating;
}
