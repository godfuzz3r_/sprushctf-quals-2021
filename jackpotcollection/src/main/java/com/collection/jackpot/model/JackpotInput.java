package com.collection.jackpot.model;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import com.collection.jackpot.model.CasinoInput;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JackpotInput {
    private String description;
    private int prize;
    private CasinoInput casino;
}
