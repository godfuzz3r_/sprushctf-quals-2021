package com.collection.jackpot.model;

public class JackpotCollectionElem {
		private int id;
    private String jackpot;

    public JackpotCollectionElem(int id, String jackpot) {
        this.id = id;
        this.jackpot = jackpot;
    }

	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJackpot() {
        return jackpot;
    }

    public void setJackpot(String jackpot) {
        this.jackpot = jackpot;
    }
}
