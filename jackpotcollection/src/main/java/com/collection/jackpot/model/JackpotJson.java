package com.collection.jackpot.model;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.collection.jackpot.model.Casino;

public class JackpotJson {
    public String description;
    public int prize;
    @JsonTypeInfo(use = Id.CLASS)
    public Object casino;

	public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrize() {
        return prize;
    }

    public void setPrize(int prize) {
        this.prize = prize;
    }

    public Object getCasino() {
        return casino;
    }

    public void setCasino(Object casino) {
        this.casino = casino;
    }
}
