package com.collection.jackpot.model;

import com.collection.jackpot.model.Casino;

public class Jackpot {
    private String description;
    private int prize;
    private Casino casino;

    public Jackpot(){}
    public Jackpot(String description, int prize, Casino casino) {
        this.description = description;
        this.prize = prize;
        this.casino = casino;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrize() {
        return prize;
    }

    public void setPrize(int prize) {
        this.prize = prize;
    }

    public Casino getCasino() {
        return casino;
    }

    public void setCasino(Casino casino) {
        this.casino = casino;
    }
}
