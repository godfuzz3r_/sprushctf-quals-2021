package com.collection.jackpot.service;

import com.collection.jackpot.model.JackpotInput;
import com.collection.jackpot.model.Casino;
import com.collection.jackpot.model.Jackpot;
import com.collection.jackpot.model.JackpotJson;
import com.collection.jackpot.model.JackpotCollectionElem;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentLinkedQueue;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

import com.collection.jackpot.exception.ServiceSqlException;
import com.collection.jackpot.exception.JackpotNotFoundException;
import com.collection.jackpot.exception.ParseJsonException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.DeserializationFeature;

@Service
public class JackpotService {
	Connection connection = null;

	public JackpotService() {
		try{
			connection = DriverManager.getConnection("jdbc:sqlite:jackpotcollection.db");
		} catch (SQLException e){
			e.printStackTrace();
		}
	}

	public Collection<JackpotCollectionElem> getCollection() {
		Jackpot jackpot = null;
		Collection<JackpotCollectionElem> collection = new ConcurrentLinkedQueue<>();
		ResultSet rs = null;
		Statement statement = null;
		try {
			//Connection connection = DriverManager.getConnection("jdbc:sqlite:jackpotcollection.db");
			statement = connection.createStatement();
			rs = statement.executeQuery("SELECT * from collection;");
			while (rs.next()) {
				collection.add(new JackpotCollectionElem(rs.getInt("id"), rs.getString("jackpot")));
			}
		}catch (SQLException e) {
			throw new ServiceSqlException(500, "SQL error: " + e.getMessage());
		} finally {
			if(rs != null){
				try{
					rs.close();
				} catch(Exception e){
					e.printStackTrace();
				}
			}
			if(statement != null){
				try{
					statement.close();
				} catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		return Collections.unmodifiableCollection(collection);
  }

	public String getJsonJackpot(final int id){
		String jackpotStr = null;
		ResultSet rs = null;
		Statement statement = null;

		try {
			statement = connection.createStatement();
			rs = statement.executeQuery("SELECT * from collection;");
		while (rs.next()) {
			if (rs.getInt("id") == id){
				jackpotStr = rs.getString("jackpot");
				break;
			}
		}
		}catch (SQLException e) {
			throw new ServiceSqlException(500, "SQL error: " + e.getMessage());
		} finally {
			if(rs != null){
			     try{
			          rs.close();
			     } catch(Exception e){
			         e.printStackTrace();
			     }
			}
			if(statement != null){
				 try{
					  statement.close();
				 } catch(Exception e){
					 e.printStackTrace();
				 }
			}
		}

    if (jackpotStr == null){
      throw new JackpotNotFoundException(404, "Unable to find jackpot with id " + Integer.toString(id));
    }

    return jackpotStr;
	}

	public JackpotCollectionElem addJackpot(final String jsonjackpot) {
		PreparedStatement crsStmt = null;
		ResultSet rs = null;
		Statement statement = null;
		try {
			//Connection connection = DriverManager.getConnection("jdbc:sqlite:jackpotcollection.db");
			statement = connection.createStatement();
			statement.executeUpdate("CREATE TABLE IF NOT EXISTS collection (id INTEGER, jackpot TEXT, PRIMARY KEY(id))");

			crsStmt = connection.prepareStatement("INSERT INTO collection VALUES (?, ?)");
			crsStmt.setString(2, jsonjackpot);
			crsStmt.executeUpdate();

			rs = crsStmt.getGeneratedKeys();
			int last_inserted_id = rs.getInt(1);
			return new JackpotCollectionElem(last_inserted_id, jsonjackpot);
		}catch(SQLException e){
			throw new ServiceSqlException(500, "SQL error: " + e.getMessage());
		} finally {
			if(rs != null){
				try{
					rs.close();
				} catch(Exception e){
					e.printStackTrace();
				}
			}

			if(statement != null){
				try{
					statement.close();
				} catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}

	public String deleteJackpot(final int id){
		PreparedStatement crsStmt = null;
		try{
			getJsonJackpot(id);
		}catch(JackpotNotFoundException e){
			return "Not found.";
		}

		try {
			Statement statement = connection.createStatement();
			crsStmt = connection.prepareStatement("DELETE FROM collection WHERE id = ?");
			crsStmt.setInt(1, id);
			crsStmt.executeUpdate();
			return "Ok.";
		}catch(SQLException e){
			throw new ServiceSqlException(500, "SQL error: " + e.getMessage());
		} finally {
			if(crsStmt != null){
				try{
					crsStmt.close();
				} catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}

	public Jackpot editJackpot(final int id, final JackpotInput jackpot) {
		ObjectMapper om = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);;
		JackpotJson dbJson = null;

		System.out.println(jackpot.getDescription());

		try{
			dbJson = om.readValue(getJsonJackpot(id), JackpotJson.class);
		} catch(JackpotNotFoundException e){
			throw new ParseJsonException(404, "Jackpot not found.");
		} catch(java.io.IOException e){
			throw new ParseJsonException(500, "Parse json error: " + e.getMessage());
		}

		try{
			dbJson.setDescription(jackpot.getDescription());
		}catch (NullPointerException e) {
        	System.out.println(e.getMessage());
    	}
		try{
			dbJson.setPrize(jackpot.getPrize());
		}catch (NullPointerException e) {
        	System.out.println(e.getMessage());
    	}
		try{
			dbJson.casino = jackpot.getCasino();
		}catch (NullPointerException e) {
        	System.out.println(e.getMessage());
    	}

		PreparedStatement crsStmt = null;
		ResultSet rs = null;
		Statement statement = null;

		try {
			statement = connection.createStatement();
			crsStmt = connection.prepareStatement("UPDATE collection SET jackpot=? WHERE id = ?");
			crsStmt.setString(1, om.writeValueAsString(dbJson));
			crsStmt.setInt(2, id);
			crsStmt.executeUpdate();
		}catch(SQLException e){
			throw new ServiceSqlException(500, "SQL error: " + e.getMessage());
		}catch(java.io.IOException e){
   			throw new ParseJsonException(500, "Parse json error: " + e.getMessage());
   		} finally {
			if(crsStmt != null){
				try{
					crsStmt.close();
				} catch(Exception e){
					e.printStackTrace();
				}
			}
		}

		try{
			return om.readValue(getJsonJackpot(id), Jackpot.class);
		}catch(java.io.IOException e){
   			throw new ParseJsonException(500, "Parse json error: " + e.getMessage());
   		}
	}
}
