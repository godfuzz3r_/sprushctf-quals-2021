package com.collection.jackpot.query;

import java.util.Collection;

import com.collection.jackpot.exception.ParseJsonException;

import com.collection.jackpot.model.Casino;
import com.collection.jackpot.model.Jackpot;
import com.collection.jackpot.service.JackpotService;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.core.JsonProcessingException;


@Service
public class JackpotQueryResolver implements GraphQLQueryResolver {

    private final JackpotService jackpotService;

    public JackpotQueryResolver(final JackpotService jackpotService) {
        this.jackpotService = jackpotService;
    }

    public Jackpot jackpot(final int id) {
      Jackpot jackpot = null;
      String jackpotJson = jackpotService.getJsonJackpot(id);
      ObjectMapper om = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);;
      om.enableDefaultTyping();
      try{
        jackpot = om.readValue(jackpotJson, Jackpot.class);
        return jackpot;
      } catch(JsonProcessingException e){
        throw new ParseJsonException(500, e.getMessage());
      } catch(java.io.IOException e){
        throw new ParseJsonException(500, e.getMessage());
      }
    }
}
