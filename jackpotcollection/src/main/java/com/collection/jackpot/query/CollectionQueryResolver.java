package com.collection.jackpot.query;

import java.util.Collection;

import com.collection.jackpot.model.JackpotCollectionElem;
import com.collection.jackpot.service.JackpotService;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.stereotype.Service;

@Service
public class CollectionQueryResolver implements GraphQLQueryResolver {

    private final JackpotService jackpotService;

    public CollectionQueryResolver(final JackpotService jackpotService) {
        this.jackpotService = jackpotService;
    }

    public Collection<JackpotCollectionElem> collection() {
        return jackpotService.getCollection();
    }
}
