package com.collection.jackpot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Bean;


@EnableAutoConfiguration
@ComponentScan
public class JackpotApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(JackpotApplication.class, args);
    }

}
